# Use Cases

Compilation of useful cases done that you can adapt to your needs or get ideas to build your own labs using IsardVDI.

In the **guests** folder you'll find virtual desktops OS installations for Linux and Windows that are optimized to be optimal in a virtualization system.

In the **labs** folder we've compiled virtual scenarios that can be helpful to see the IsardVDI capabilities.