# Casos de uso

Compilación de casos útiles que se pueden adaptar a vuestras necesidades u obtener ideas para construir vuestros propios laboratorios utilizando IsardVDI.

En la carpeta **guests** encontraréis las instalaciones del sistema operativo de escritorios virtuales para Linux y Windows que están optimizadas para que sean óptimas en un sistema de virtualización.

En la carpeta **labs** hemos compilado escenarios virtuales que pueden ser útiles para ver las capacidades de IsardVDI.