# Plantillas

Una plantilla es un escritorio preconfigurado. Su disco no se puede modificar, de forma que no se pueden iniciar como escritorios, pero sus parámetros se pueden personalizar para satisfacer las necesidades de cada usuario.

Las plantillas están diseñadas para ser compartidas con otros usuarios para que puedan crear sus propios escritorios.

!!! Info "Eliminar plantillas"
    Cuando se crea una plantilla desde un escritorio, su disco se duplica. Cuando se crea un escritorio nuevo a partir de una plantilla, se genera un disco nuevo en el almacenamiento que depende del disco de la plantilla original. Por eso **los usuarios avanzados no pueden eliminar plantillas**, ya que podría provocar la pérdida de todos los escritorios que dependen de éstas.

Este es un ejemplo que ilustra la relación de las plantillas y los discos:

**1.** Se crea un escritorio con el disco de almacenamiento **D1**.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1]):::dk
 classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
 classDef dt stroke-width:2px
```

**2.** A continuación, se crea una plantilla a partir de este escritorio. Al crear la plantilla, el disco **D1** se asociará con la plantilla nueva. Al mismo tiempo, se hace una copia de **D1** y se denomina **D1'**. Este disco copiado será utilizado por el escritorio original en el futuro, de forma que cualquier cambio hecho al disco original no le afectará.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1']):::dk;
 dt1 --> tp1(Plantilla):::tp
 tp1 -.- dk2([D1]):::dk
 classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
 classDef dt stroke-width:2px
 classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**3.** Si se crea un escritorio nuevo a partir de esta plantilla, se crea un disco nuevo **D2** para el escritorio nuevo. Este disco **D2** contiene los cambios que se harán al escritorio respecto al disco de la plantilla **D1**. En otras palabras, **D2** está enlazado a **D1** y el escritorio obtendrá su información de **D1** en el momento de iniciarlo.

En decir, **D2** solo contiene los cambios hechos a **D1** que son relevantes para el escritorio nuevo, y los dos discos permanecen conectados entre sí.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1']):::dk;
 dt1 --> tp1(Plantilla):::tp
 tp1 -.- dk2([D1]):::dk
 tp1 --> dt2(Escritorio):::dt
 dt2 -.- dk3([D2]):::dk
 dk3 -- depende de --> dk2
 classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
 classDef dt stroke-width:2px
 classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**4.** Al duplicar una plantilla, no se creará ningún disco nuevo. En lugar de esto, la nueva plantilla usará el mismo disco **D1** que la plantilla original.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1']):::dk;
 dt1 --> tp1(Plantilla):::tp
 tp1 -.- dk2([D1]):::dk
 tp1 --> tp2(Plantilla):::tp
 tp2 -.- dk2
 classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
 classDef dt stroke-width:2px
 classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

Entender la relación entre plantillas, escritorios y discos es importante para poder gestionar mejor la infraestructura de escritorios virtual.

## Crear 

Para crear una plantilla a partir de un escritorio se pulsa el icono siguiente:

![](./templates.es.images/templates2.png){width="80%"}

Se le puede asignar cualquier nombre/descripción, escoger si se quiere habilitar/deshabilitar (hacerla visible/invisible), y compartirla con grupos/usuarios

![](./templates.es.images/templates5.png){width="80%"}


## Tus Plantillas

Para ver las plantillas que has creado, se tiene que dirigir al apartado de tus plantillas personales.

![](./templates.es.images/templates8.png){width="80%"}


## Compartidas contigo

En este apartado se pueden ver las plantillas que han sido compartidas con tu usuario.

![](./templates.es.images/templates9.png){width="80%"}

### Duplicar plantilla

Para duplicar una plantilla y hacerla tuya, al apartado **Plantillas compartidas contigo**, se selecciona la plantilla pulsando el icono![](./templates.images/templates14.png){width="3%"}

Se redirige a la página donde poder duplicarla.

![](./templates.es.images/templates6.png){width="80%"}

!!! Info "Importante"
        La duplicación de una plantilla compartida crea una **copia** donde el usuario es el propietario. Esto le permite **personalizar** la plantilla, incluyendo la modificación de los usuarios con los cuales se comparte.

## Editar

Para editar una plantilla, al apartado **Tus plantillas** se pulsa el icono ![](./templates.es.images/templates10.png){width="3%"}, donde redirige a la página donde poder editar la información de la plantilla (mismo fomulario y opciones como cuando se **[edita un escritorio](../user/edit_desktop.es.md)**)


## Compartir

Para compartir una plantilla, al apartado **Tus plantillas** se pulsa el icono ![](./templates.es.images/templates13.png){width="3%"} y saldrá una ventana donde se podrán asignar los grupos y/o usuarios.

![](./templates.es.images/templates11.png){width="60%"}


## Hacer visible o invisible

Para modificar la visibilidad de una plantilla, en el apartado **Tus plantillas**, se hace click en el botón ![](./templates.ca.images/templates15.png){width="3%"} o el botón ![](./templates.ca.images/templates16.png){width="3%"} dependiendo de la visibilidad actual de ésta.

**El estado del ojo determinará la visibilidad de la plantilla**:

- Ojo abierto y botón azul ![](./templates.ca.images/templates15.png){width="3%"}: plantilla visible
- Ojo negado y botón gris ![](./templates.ca.images/templates16.png){width="3%"}: plantilla invisible

Aparecerá un formulario donde aceptar o negar el cambio de la visibilidad de la plantilla:

![](./templates.es.images/templates17.png){width="25%"}
![](./templates.es.images/templates19.png){width="25%"}