# Media

In this section you will see the media created by you and those shared with you.

Press the button ![](./media.images/media1.png)

![](./media.images/media2.png)


## Your Media

In this section you can see the media that the user has uploaded.

![](./media.images/media3.png)


## Shared with you

In this section you can see the media that have been shared with the user.

![](./media.images/media4.png)


## Upload Iso

To upload an iso, press the button ![](./media.images/media5.png)

![](./media.images/media6.png)

And it redirects to a page where you fill in the fields:

![](./media.images/media7.png)


## Share

To share an iso, press the icon ![](./media.es.images/media10.png)

And a dialog box will appear where you can select if you want to share by groups/users

![](./media.images/media10.png)


## Generate desktop from media

To generate a desktop from an iso, press the icon ![](./media.es.images/media7.png)

![](./media.images/media8.png)

And it redirects you to a page where you can fill in the form with the name, viewers, etc.

![](./media.images/media9.png)


## Delete an iso

To delete an iso, press the button ![](./media.es.images/media12.png)

![](./media.images/media11.png)