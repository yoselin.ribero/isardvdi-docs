# Plantilles

Una plantilla és un escriptori preconfigurat. El seu disc no es pot modificar, de manera que no es poden iniciar com escriptoris, però els seus paràmetres es poden personalitzar per satisfer les necessitats de cada usuari.

Les plantilles estan dissenyades per ser compartides amb altres usuaris perquè puguin crear els seus propis escriptoris.

!!! Info "Eliminar plantilles"
    Quan es crea una plantilla des d'un escriptori, el seu disc es duplica. Quan es crea un escriptori nou a partir d'una plantilla, es genera un disc nou en l'emmagatzematge que depèn del disc de la plantilla original. Per això **els usuaris avançats no poden suprimir plantilles**, ja que podria provocar la pèrdua de tots els escriptoris que en depenen.

Aquest és un exemple que il·lustra la relació de les plantilles i els discs:

**1.** Es crea un escriptori amb el disc d'emmagatzematge **D1**.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1]):::dk
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
  classDef dt stroke-width:2px
```

**2.** A continuació, es crea una plantilla a partir d'aquest escriptori. En crear la plantilla, el disc **D1** s'associarà amb la plantilla nova. Al mateix temps, es fa una còpia de **D1** i s'anomena **D1'**. Aquest disc copiat serà utilitzat per l'escriptori original en el futur, de manera que qualsevol canvi fet al disc original no l'afectarà.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**3.** Si es crea un escriptori nou a partir d'aquesta plantilla, es crea un disc nou **D2** per a l'escriptori nou. Aquest disc **D2** conté els canvis que es faran a l'escriptori respecte al disc de la plantilla **D1**. En altres paraules, **D2** està enllaçat a **D1** i l'escriptori obtindrà la seva informació de **D1** en el moment d'iniciar-lo.

Ës a dir, **D2** només conté els canvis fets a **D1** que són rellevants per a l'escriptori nou, i els dos discs romanen connectats entre si.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> dt2(Escriptori):::dt
  dt2 -.- dk3([D2]):::dk
  dk3 -- depèn de --> dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**4.** En duplicar una plantilla, no es crearà cap disc nou. En lloc d'això, la nova plantilla farà servir el mateix disc **D1** que la plantilla original.

``` mermaid
graph LR
  dt1(Escriptori):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Plantilla):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> tp2(Plantilla):::tp
  tp2 -.- dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

Entendre la relació entre plantilles, escriptoris i discs és important per poder gestionar millor la infraestructura d'escriptoris virtual.

## Crear 

Per a crear una plantilla a partir d'un escriptori es prem la icona següent:

![](./templates.ca.images/templates1.png){width="80%"}

Se li pot assignar qualsevol nom/descripció, triar si es vol habilitar/deshabilitar (fer-la visible/invisible), i compartir-la amb grups/usuaris

![](./templates.ca.images/templates3.png){width="80%"}


## Les teves plantilles

Per a veure les plantilles que has creat, el docent s'ha de dirigir a l'apartat de les seves plantilles personals.

![](./templates.ca.images/templates4.png){width="80%"}


## Compartides amb tu

En aquest apartat es poden veure les plantilles que han estat compartides amb el teu usuari.

![](./templates.ca.images/templates6.png){width="80%"}

### Duplicar plantilla

Per a duplicar una plantilla i fer-la teva, a l'apartat **Plantilles compartides amb tu**, es selecciona la plantilla prement l'icona ![](./templates.images/templates14.png){width="3%"}

Es redirigeix a la pàgina on poder duplicar-la.

![](./templates.ca.images/templates14.png){width="80%"}

!!! Info "Important"
    La duplicació d'una plantilla compartida crea una **còpia** on l'usuari és el propietari. Això li permet **personalitzar** la plantilla, incloent-hi la modificació dels usuaris amb els quals es comparteix.

## Editar

Per a editar una plantilla, a l'apartat **Les teves plantilles** es prem la icona ![](./templates.es.images/templates10.png){width="3%"}, on redirigeix a la pàgina on poder editar la informació de la plantilla (mateix formulari i opcions com quan s'**[edita un escriptori](../user/edit_desktop.ca.md)**).


## Compartir

Per a compartir una plantilla, a l'apartat **Les teves plantilles** es prem la icona ![](./templates.es.images/templates13.png){width="3%"} i sortirà una finestra on es podran assignar els grups i/o usuaris.

![](./templates.ca.images/templates10.png){width="60%"}


## Fer visible o invisible

Per modificar la visibilitat d'una plantilla, a l'apartat **Les teves plantilles**, es prem la icona ![](./templates.ca.images/templates15.png){width="3%"} o la icona ![](./templates.ca.images/templates16.png){width="3%"} depenent de la visibilitat actual d'aquesta.

**L'estat de l'ull determinarà la visibilitat de la plantilla**:

- Ull obert i botó blau ![](./templates.ca.images/templates15.png){width="3%"}: plantilla visible
- Ull negat i botó gris ![](./templates.ca.images/templates16.png){width="3%"}: plantilla invisible

Apareixerà un formulari on acceptar o negar el canvi de la visibilitat de la plantilla:

![](./templates.ca.images/templates17.png){width="25%"}
![](./templates.ca.images/templates18.png){width="25%"}
