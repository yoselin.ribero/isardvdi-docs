# Templates

A template is a pre-configured desktop. Their disk can't be modified, so they can't be started like regular desktops, but their parameters can be customized to suit the needs of individual users.

Templates are designed to be shared with other users so they can create their own desktops.

!!! Info "Deleting templates"
    When creating a template from a desktop, its disk is duplicated. When you create a new desktop from a template, a new disk is generated in storage that depends on the disk of the original template. That is why **advanced users aren't able to delete templates**, as it could result in the loss of all the desktops that depend on it.

Here is an example that illustrates the relation of templates and disks:

**1.**  A desktop is created with storage disk **D1**.

<!-- Figure 1 -->

``` mermaid
graph LR
  dt1(Desktop):::dt -.- dk1([D1]):::dk
  classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
  classDef dt stroke-width:2px
```

**2.**  Next, a template is created from this desktop. When creating the template the disk **D1** becomes associated with the new template. At the same time, a copy of **D1** is made and named **D1'**. This copied disk will be used by the original desktop in the future, so that any changes made to the original disk won't affect it.

<!-- Figure 2 -->

``` mermaid
graph LR
  dt1(Desktop):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Template):::tp
  tp1 -.- dk2([D1]):::dk
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**3.**  If a new desktop is created from this template, a new disk **D2** is created for the new desktop. This disk **D2** contains the changes that will be made to the desktop in regard to the template's disk **D1**. In other words, **D2** is linked to **D1** and the desktop gets its information from **D1** at the moment of starting it.

To simplify, **D2** only contains the changes made to **D1** that are relevant to the new desktop, and both disks remain connected to each other.

<!-- Figure 3 -->

``` mermaid
graph LR
  dt1(Desktop):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Template):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> dt2(Desktop):::dt
  dt2 -.- dk3([D2]):::dk
  dk3 -- depends on --> dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**4.**  When duplicating a template, no new disk will be created. Instead, the new template will use the same disk **D1** as the original template.

<!-- Figure 4 -->

``` mermaid
graph LR
  dt1(Desktop):::dt -.- dk1([D1']):::dk;
  dt1 --> tp1(Template):::tp
  tp1 -.- dk2([D1]):::dk
  tp1 --> tp2(Template):::tp
  tp2 -.- dk2
  classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
  classDef dt stroke-width:2px
  classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

By understanding the relationship between templates, desktops, and disks, you can better manage your virtual desktop infrastructure.

## Create 

To create a template from a desktop, click the following icon:

![](./templates.images/templates2.png){width="80%"}

You can assign any name/description, choose whether to enable/disable it (make it visible/invisible), and share it with groups/users

![](./templates.images/templates3.png){width="80%"}


## Your Templates

To see the templates that you have created, you have to go to the section of your personal templates.

![](./templates.images/templates4.png){width="80%"}


## Shared with you

In this section you can see the templates that have been shared with your user.

![](./templates.images/templates6.png){width="80%"}


### Duplicate template

To duplicate a template and make it yours, in **Templates shared with you** section, select the template by clicking the icon ![](./templates.images/templates14.png){width="3%"}

And it redirects you to the page where you can duplicate it.

![](./templates.images/templates15.png){width="80%"}

!!! Info "Important"
    Duplicating a shared template creates a **copy** where the user is the owner. This allows you to **customize** the template, including changing the users with whom it is shared.


## Edit

To edit a template, in **Your templates** section, click on the icon ![](./templates.es.images/templates10.png){width="3%"}, where it redirects to the page where you can edit the template information (same form and options as when you **[edit a desktop](../user/edit_desktop.md)**)


## Share

To share a template, in **Your templates** section, click the icon ![](./templates.es.images/templates13.png){width="3%"} and a window will appear where you can assign the groups and/or users.

![](./templates.images/templates10.png){width="60%"}


## Make Visible or Invisible

To modify the visibility of a template, in **Your templates** section, click on the button ![](./templates.ca.images/templates15.png){width="3%"} or the ![](./templates.ca.images/templates16.png){width="3%"} button depending on its current visibility.

**The state of the eye will determine the visibility of the template**:

- Open eye and blue button ![](./templates.ca.images/templates15.png){width="3%"}: visible template
- Eye denied and gray button ![](./templates.ca.images/templates16.png){width="3%"}: invisible template

A form will appear where you can accept or deny the change of the visibility of the template:

![](./templates.images/templates11.png){width="25%"}
![](./templates.images/templates12.png){width="25%"}