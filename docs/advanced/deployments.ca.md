# Desplegaments

!!! Info
    Aquesta secció és només per a **administradors**, **gestors** i **usuaris avançats**

Els desplegaments són una característica potent que permet als usuaris avançats crear un conjunt d'escriptoris basats en una única plantilla. Tots els usuaris inclosos en un desplegament tindran accés al seu propi escriptori únic.

Amb els desplegaments, podeu crear i gestionar diversos escriptoris simultàniament per a tots els usuaris seleccionats. Com a creador, podeu gestionar fàcilment tots els escriptoris en un sol lloc a través de la secció Desplegaments. Fins i tot teniu la capacitat d'interactuar amb cada escriptori i veure les pantalles simultàniament.


## Crear un desplegament

Per a crear un desplegament, a l'apartat ![Botó superior de desplegaments](deployments.ca.images/deployments1.png){width="11%"} s'ha de prémer el botó ![Desplegament nou](deployments.ca.images/deployments3.png){width="11%"}

Se us dirigirà a un formulari on es pot introduir la informació necessària per al vostre desplegament, inclòs el nom del desplegament, el nom de l'escriptori que veuran els usuaris i una breu descripció.

![formulari de desplegament nou](deployments.ca.images/deployments6.png)

Allí, s'ha de seleccionar una plantilla des d'on crear els vostres escriptoris. Un vegada creat, els escriptoris es faran visibles per als usuaris, prement a ![no visible / visible](deployments.ca.images/deployments5.png)

A més, heu d'especificar els usuaris que s'han d'incloure en el desplegament. A la secció final del formulari, podeu seleccionar el grup necessari d'usuaris, així com qualsevol usuari addicional.

L'última secció són les opcions avançades. Aquí podeu canviar els visualitzadors dels escriptoris, el maquinari i la miniatura.

!!! important Important
        1. El **propietari** del desplegament és **l'únic** que pot editar el seu escriptori **individualment** sense que afecti als altres escriptoris.
        2. També es crearà un escriptori per al vostre usuari dins del desplegament


## Opcions d'un desplegament

### Arrencar i aturar

Per accedir al desplegament, es prem al nom del desplegament.

![llista de desplegaments](deployments.ca.images/deployments7.png)

Per a iniciar un escriptori, es prem el botó ![Iniciar](deployments.ca.images/deployments8.png)

Un cop iniciat, detectarà la "IP" de l'escriptori (si en té), l'"Estat" (si està iniciat o aturat), i el tipus de visor es pot seleccionar prement el menú desplegable

![llista desplegable dels visors](deployments.ca.images/deployments11.png){width="30%"}

Per aturar un escriptori iniciat, simplement es prem el botó ![aturar](deployments.ca.images/deployments14.png) que apareixerà. Això canviarà el seu estat a "Aturant". Si bé es pot utilitzar l'opció "Força l'aturada" per a una parada immediata, però pot provocar problemes de tancament i problemes potencials.

Per a aturar tots els escriptoris, es prem la icona ![icona d'aturada](deployments.ca.images/deployments16.png)


### Fer visible o invisible

Per canviar la visibilitat dels escriptoris d'un desplegament, s'ha de seguir la mateixa funcionalitat que **[la visibilitat de les plantilles](../templates.ca/#fer-visible-o-invisible)**, fent ús de la mateixa icona.

![fer invisible](deployments.ca.images/deploy-accion1.png){width="60%"}

![fer visible](deployments.ca.images/deploy-accion2.png){width="60%"}

Hi ha dues opcions per triar:

- **No aturis els escriptoris**: Aquesta opció farà que el desplegament sigui invisible per a l'usuari. No obstant això, si un usuari està utilitzant actualment el seu escriptori en el moment en què es fa invisible, encara podran utilitzar-lo fins que decideixin aturar-lo. Un cop aturat, l'usuari no podrà accedir-hi de nou fins que el desplegament es torni a fer visible.

- **Atura els escriptoris**: Aquesta opció també farà invisible el desplegament. No obstant això, si qualsevol usuari ja està utilitzant el seu escriptori, li apagarà immediatament.

Un cop seleccionat "Invisible", el desplegament es tornarà vermell

El desplegament mostrarà el seu estat en **verd** o en **vermell**, si és **visible** o **invisible** respectivament:

![](deployments.ca.images/deployments24.png){width="45%"}
![](deployments.ca.images/deployments25.png){width="44%"}

### Videowall

El Videowall és una funció que permet veure totes les pantalles de tots els escriptoris d'un desplegament particular simultàniament i en temps real. També permet interactuar amb qualsevol dels escriptoris mostrats i utilitzar-los com a propis.

!!! Warning
    Entrar a la pantalla d'un altre usuari prendrà el control de l'usuari propietari.
  
!!! Danger "Avís legal"
    Tingueu en compte que el permís del propietari és essencial abans d'entrar al seu escriptori a causa de problemes de privadesa.

Per accedir al Videowall, es prem la icona ![icona del videowall](deployments.ca.images/deployments18.png)

Redirigirà a una pàgina on es pot veure els escriptoris de tots els usuaris.

![pantalla del videowall](deployments.ca.images/deployments20.png)

Per a interactuar amb un escriptori, es prem la icona ![maximitza](deployments.ca.images/deployments21.png) que redirigirà a una vista a pantalla completa de l'escriptori seleccionat a pantalla completa.

![pantalla completa del videowall](deployments.ca.images/deployments22.png)

!!! Warning "Visors RDP"
    El videowall no mostrarà escriptoris que facin servir visors RDP.

### CSV de visors directes

Aquest fitxer s'utilitza per veure la llista d'usuaris al desplegament i els seus respectius enllaços al [visor directe](../login/direct_viewer_access.ca.md) de l'escriptori. 
Seguiu els passos següents per a descarregar aquest fitxer:

Primer, es prem la icona ![icona de descàrrega](deployments.ca.images/deployments34.png)

Apareixerà una notificació:

![notificació](deployments.ca.images/deployments35.png)

S'ha de confirmar la notificació per baixar el fitxer CSV.

Aquest fitxer conté cada nom d'usuari, el seu nom, el seu correu electrònic i un enllaç al visor directe que redirigeix a l'escriptori de l'usuari.

![contingut del fitxer csv](deployments.ca.images/deployments36.png)

### Recrear escriptoris

La funció "Recrear escriptoris" pretén modificar el desplegament afegint o eliminant escriptoris basats en els canvis fets als permisos de l'usuari. Si s'afegeix o elimina un usuari, aquesta funció actualitzarà el desplegament afegint o eliminant els seus escriptoris respectius .

Per fer-ho, es prem la icona ![icona recrear](deployments.ca.images/deployments31.png)

![notificació](deployments.ca.images/deployments33.png){width="30%"}


### Editar desplegament

Per a editar un desplegament, es pot fer de dues maneres prement la icona ![](../advanced/deployments.ca.images/deployments9.png):

1. Des del llistat de desplegaments.

    ![](../advanced/deployments.ca.images/deployments41.png){width="80%"}

2. Des de la vista de gestió de desplegaments.

    ![](../advanced/deployments.ca.images/deployments42.png){width="80%"}

I redirigirà a la pàgina d'edició.

En aquesta pàgina es pot editar tots els elements (els mateixos que tens al crear el desplegament)

![](../advanced/deployments.ca.images/deployments39.png){width="80%"}


### Editar usuaris

Per a editar els usuaris del desplegament, es pot fer de dues maneres prement la icona ![](../advanced/deployments.ca.images/deployments10.png) 

1. Des del llistat de desplegaments.

    ![](../advanced/deployments.ca.images/deployments43.png){width="80%"}

2. Des de la vista de gestió de desplegaments.

    ![](../advanced/deployments.ca.images/deployments44.png){width="80%"}

I sortirà una finestra on es podran assignar els grups i/o usuaris.

![](../advanced/deployments.ca.images/deployments40.png){width="80%"}


### Esborrar 

#### Desplegament

Per a esborrar un desplegament, es pot fer de dues maneres prement la icona ![icona de la paperera](deployments.ca.images/deployments37.png) 

!!! note Nota
        S'eliminaran tots els escriptoris creats.

1. Des del llistat de desplegaments.

    ![](../advanced/deployments.ca.images/deployments46.png){width="80%"}

2. Des de la vista de gestió de desplegaments.

    ![](../advanced/deployments.ca.images/deployments45.png){width="80%"}

I sortirà un missatge on de si estàs d'acord en esborrar el desplegament.

![](../advanced/deployments.ca.images/deployments38.png){width="30%"}

#### Usuaris del desplegament

Per a esborrar un usuari desplegament, es pot fer de dues maneres prement la icona ![icono de la papelera](./deployments.ca.images/deployments37.png) 

![](../advanced/deployments.ca.images/deployments47.png){width="80%"}

!!! note Nota
        Per a recuperar l'usuari esborrat, s'ha de prémer el botó de [recrear](../advanced/deployments.ca.md/#recrear-escriptoris)


## Reservar desplegament

Abans de crear una reserva per primer cop, s'ha de llegir l'[apartat de reserves del manual](../../user/bookings.ca) on s'explica millor què són les reserves i per què són necessàries amb escriptoris amb GPU.


### Crear una reserva

S'accedeix a reservar una GPU pel desplegament des de la última icona d'accions d'aquest.

![](../../user/bookings.images/deployments_panel_schedule.png)

Dóna accés a la vista setmanal de la **disponibilitat** (es pot canviar la vista a mensual o diària), on es troben dues columnes per cada dia de la setmana. En la columna de l'esquerra surt la disponibilitat pel perfil de targeta, i en la columna de la dreta és on sortiran les reserves que hi han creades per a aquest desplegament.

En la figura s'aprecia que existeix **disponibilitat** durant la setmana i no hi ha feta **cap** reserva.

![](../../user/book_desktop.ca.images/2.png)

Les reserves poden crear-se mitjançant el botó de dalt a la dreta ![](../../user/book_desktop.ca.images/3.png) o mitjançant el cursor, **clicant en la franja Reserves i arrossegant** per a seleccionar el rang d'hora desitjat. En el formulari que apareixerà s'ajusten el rang de dates i hores de durada de la reserva que volem realitzar.

![](../../user/book_desktop.ca.images/4.png){width="60%"}

Una vegada feta la reserva, sortirà a la columna de la dreta de cada jornada. 

![](../../user/book_desktop.ca.images/5.png){width="20%"}


### Esborrar reserva

Per a eliminar una reserva, es prem sobre la franja i mitjançant el botó d'eliminació que surt al editar-la.

![](../../user/book_desktop.ca.images/6.png){width="60%"}
![](../../user/book_desktop.ca.images/7.png){width="30%"}

![](../../user/book_desktop.ca.images/8.png){width="20%"}


### Videotutorials

Aquí oferim dos videotutorials que expliquen les configuracions prèvies d'un escriptori o desplegament, i com realitzar reserves per ambdues opcions, amb els **dos mètodes de reserva que existeixen a IsardVDI**, a la primera part i segona part respectivament. 

Subtítols en català disponibles.

<iframe width="560" height="315" src="https://www.youtube.com/embed/9ZnSCFQ7I_s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tvkd4OE26y4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
