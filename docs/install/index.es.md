# Inicio rápido

Sigue estas instrucciones para clonar el repositorio y activar IsardVDI:

```bash
git clone https://gitlab.com/isard/isardvdi
cd isardvdi
cp isardvdi.cfg.example isardvdi.cfg
./build.sh
docker-compose pull
docker-compose up -d
```

Espere un minuto la primera vez, ya que la base de datos se completará antes de que el inicio de sesión de IsardVDI esté disponible.

Navega a <https://localhost>. El usuario predeterminado es **admin** y la contraseña predeterminada es **IsardVDI**.

Se puede descargar inmediatamente imágenes del sistema operativo preinstaladas y optimizadas desde el menú *Descargas* y usarlas, crear plantillas y mucho más.

Por favor, lea el resto de la documentación para disfrutar de todas las funciones.