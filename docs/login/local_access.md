# Local Access

If an **available category** has been acquired to the platform, managed by the administrators or managers, select the **language** and enter the **user's credentials** (you can choose the **category in the dropdown menu** below the language or by typing in the access link **https://domain_isardvdi/login/category_name**).

![](./local_access.images/local_access1.png){width="80%"}

!!! note
    The choice of **language** is saved in the **browser cookies**, so in the user's next login, it will already be **preselected**.