# Dominios

Para ir a la sección de "Domains", se pulsa el botón ![](./users.es.images/users1.png)

![](./users.es.images/users2.png)

Y se pulsa el menú desplegable ![](./domains.images/domains1.png)

![](./domains.images/domains2.png)


## Escritorios

!!! info "Roles con acceso"

    Solo los **administradores** tienen acceso a todas estas características.

En esta sección se pueden ver todos los escritorios que se hayan creado de todas las categorías.

![](./domains.images/domains4.png)

Si se pulsa el icono ![](./domains.images/domains5.png), se puede ver más información del escritorio.


* Status detailed info: aquí se muestra el estado del escritorio. En caso de escritorio fallado, se mostrará un mensaje de error que puede ayudar a identificar el problema

* Description: descripción del escritorio

* Template tree: una representación visual que muestra la jerarquía de escritorios y la plantilla de donde se derivan estos escritorios y sus dependencias. Esto es útil para comprender el origen de un escritorio en particular

* Hardware: como su nombre indica, es el hardware que se ha asignado a este escritorio

* Storage: indica el UUID del disco asociado y su uso

* Media: indica que los medios asociados al escritorio, si hay alguno

![](./domains.images/domains6.png)

### Global actions

En la parte superior de la tabla tenemos un desplegable con varias acciones. Estas acciones se aplicarán a todos los escritorios seleccionados.

![Desplegable de acciones globales](./domains.images/domains54.png)

Para seleccionar un escritorio, simplemente se tiene que hacer clic en la fila de la tabla. Los escritorios seleccionados aparecerán como marcados en la casilla de "selected" al final de la fila. Aunque se navegue a la página siguiente, los escritorios seleccionados permanecerán marcados.

![Checkboxes de selección](./domains.images/domains55.png)

Alternativamente, si no se selecciona ningún escritorio, se pueden utilizar los filtros de arriba o debajo de la tabla para filtrarlos.

!!! Warning Aviso
    Si no se selecciona ningún escritorio o aplicáis ningún filtro, todos los escritorios se seleccionarán por defecto.

Una vez se hayan seleccionado los escritorios, se elige una acción del desplegable para realizarla.

<!-- * Soft toggle started/shutting-down state:

* Toggle started/stopped state:

* Soft shut down:

* Shut down:

* Force Failed state:

* Delete:

* Start Paused (check status):

* Remove forced hypervisor:

* Remove favourite hypervisor: -->

### Bulk Edit Desktops

Se pueden seleccionar varios escritorios haciendo clic en las filas de la tabla. De manera alternativa, se pueden filtrar los escritorios sin hacer clic en las filas y todos los escritorios que se muestran en la tabla contarán como seleccionados, igual que se hace con Global Actions.

Al hacer clic en el botón ![Bulk Edit Desktops](./domains.images/domains40.png) aparecerá una ventana:

![(Ventana de actualizar múltiples escritorios a la vez)](./domains.images/domains41.png)

* Desktops to edit: Indica todos los escritorios que se actualizarán con los datos del formulario. En caso de haber muchos escritorios, se puede ir desplazando por la lista para seleccionarlos."

* Desktops viewers: si está marcada, aparecerá una sección nueva. Los visores de los escritorios se actualizarán según esté seleccionado.

![Update viewers section](./domains.images/domains42.png)

Si está marcada la opción "Update RDP credentials", también se cambiará el nombre de usuario y la contraseña utilizados para las sesiones RDP.

![(Update RDP credentials)](./domains.images/domains43.png)

* Hardware: los datos solo se cambiarán en los campos donde el valor seleccionado es diferente a "--"

Si está marcada la opción "Update network", aparecerá una sección nueva. Las interfaces de los escritorios seleccionadas se actualizarán, usando la tecla CTRL mientras se hace clic a las opciones para seleccionar más de una interfaz a la vez.

![(Update network)](./domains.images/domains44.png)

* Bookable resources: Los reservables se actualizarán cuando el valor seleccionado sea diferente a "--"

Para aplicar todos los cambios y actualizar los escritorios seleccionados, se tiene que hacer clic al botón ![(Modify desktops)](./domains.images/domains45.png)

### Bulk Add Desktops

Al hacer clic en ![Bulk Add Desktops](./domains.images/domains56.png) aparecerá una ventana

![(Modal de Bulk Add)](./domains.images/domains57.png)

Este formulario permite crear varios escritorios simultáneamente. Para crearlos, simplemente se tiene que rellenar el formulario especificando un nombre para los escritorios nuevos y seleccionando la plantilla sobre la que se basarán.

A la sección "Allowed", se pueden seleccionar los usuarios para los cuales se quieren crear los escritorios eligiendo entre roles, categorías, grupos o nombres de usuario. Se creará un escritorio separado para cada usuario seleccionado.


## Plantillas

En esta sección se pueden ver todas las plantillas que se hayan creado de todas las categorías.

![](./domains.images/domains7.png)

Si haces clic en el botón ![](./domains.images/domains49.png) situado en la esquina superior derecha, las plantillas que no están habilitadas (visibles para los usuarios con los que se comparte) no aparecerán en la tabla. Cuando vuelvas a hacer clic en el botón ![](./domains.images/domains50.png), se mostrarán de nuevo.

Si se pulsa el icono ![](./domains.images/domains5.png), se puede ver más información de la plantilla.

* Hardware: como su nombre indica, es el hardware que se le ha asignado a esa plantilla

* Allows: indica a quien se le ha dado permisos para utilizar la plantilla

* Storage: indica el UUID del disco asociado a la plantilla y su uso

* Media: indica los medios asociados a la plantilla, si hay alguno

![](./domains.images/domains8.png)

### Duplicados

Esta opción te permite duplicar la plantilla en la base de datos, mientras se mantiene el disco original.

La duplicación de plantillas se puede realizar varias veces y es útil para diversos casos, como:

- Se quiere compartir una plantilla en otras categorías. Se podría duplicar la plantilla y asignarla a un usuario en otra categoría con permisos de compartición separados para la copia nueva, manteniendo la versión original.

Eliminar una plantilla duplicada no eliminará la plantilla original.

Al duplicar una plantilla, se puede personalizar la nueva copia:

- Nombre de la nueva plantilla
- Descripción de la nueva plantilla
- Usuario asignado como el nuevo propietario de la plantilla
- Habilitar o deshabilitar la visibilidad de la plantilla para usuarios compartidos
- Seleccionar usuarios con quienes compartir la plantilla.

![](./domains.images/domains46.png)

#### Delete

Está opción eliminará el dominio y su disco asociado.

Al eliminar una plantilla, aparecerá un modal que mostrará todos los escritorios creados a partir de ella i cualquier plantilla duplicada. Eliminar la plantilla comportará la eliminación de todos los dominios asociados y sus discos.

![](./domains.images/domains47.png)

No obstante, si la plantilla a eliminar es una duplicada de otra plantilla, no sería necesario eliminarlas todas. Solamente se deberían de eliminar si se borra la plantilla de origen.

![](./domains.images/domains48.png)


## Media

En esta sección se pueden ver todos los archivos media que se hayan subido de todas las categorías.

![](./domains.images/domains9.png)


## Resources

En esta sección se pueden ver todos los recursos que tiene la instalación, éstos saldrán al crear un escritorio/plantilla.

![](./domains.images/domains10.png)


### Graphics

En esta sección se puede añadir diferentes tipos de visores con los que se pueden ver un escritorio.

Para añadir un visor nuevo, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains12.png)

Y se rellena el formulario

![](./domains.images/domains13.png)


### Videos

En esta sección se pueden añadir diferentes formatos de vídeo.

Para añadir un nuevo formato, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains14.png)

Y se rellena el formulario

![](./domains.images/domains15.png)

(heads -> cuantos monitores quieres)

### Interfaces

En esta sección se pueden añadir redes privadas a los escritorios.

Para añadir una red, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains16.png)

Y se rellena el formulario

* Type: Bridge , Network, OpenVSwitch, Personal

    - Bridge: Enlaza con un puente hacia una red del servidor. En el servidor se puede tener interfaces que enlacen por ejemplo con vlans hacia un troncal de tu red, y se puede mapear estas interfaces en isard y conectarlas a los escritorios.

    Para poder mapear la interfaz dentro del hipervisor, en el archivo de isardvdi.conf se tiene que modificar esta línea:

    ```
    # ------ Trunk port & vlans --------------------------------------------------
    ## Uncomment to map host interface name inside hypervisor container.
    ## If static vlans are commented then hypervisor will initiate an 
    ## auto-discovery process. The discovery process will last for 260
    ## seconds and this will delay the hypervisor from being available.
    ## So it is recommended to set also the static vlans.
    ## Note: It will create VlanXXX automatically on webapp. You need to
    ## assign who is allowed to use this VlanXXX interfaces.
    #HYPERVISOR_HOST_TRUNK_INTERFACE=

    ## This environment variable depends on previous one. When setting
    ## vlans number comma separated it will disable auto-discovery and
    ## fix this as forced vlans inside hypervisor.
    #HYPERVISOR_STATIC_VLANS=
    ```


* Model: rtl8931, virtio, e1000

    - Es más eficiente utilizar virtio (que es una interface paravirtualizada), mientras que las e1000 o rtl son simulaciones de tarjetas, y va más lento aunque es más compatible con sistemas operativos antiguos y no se necesita instalar drivers en el caso de windows.

    **Si se tiene un sistema operativo moderno, con el virtio funciona, sino con los otros que son interfaces. Windows antiguos: rtl, e1000**


* QoS: limit up and down to 1 Mbps, unlimited

![](./domains.images/domains17.png)


### Boots

En esta sección se indican los diferentes modos de arranque de un escritorio.

**Si no se tiene permisos no se puede seleccionar una iso de arranque y no se deja por defecto que los usuarios se puedan mapear isos.**

Para dar permisos se pulsa en el icono ![](./domains.images/domains25.png)

![](./domains.images/domains18.png)


### Network QoS

En esta sección se indican las limitaciones que se pueden poner a las redes.

Para añadir una limitación, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains19.png)

Y se rellena el formulario

![](./domains.images/domains20.png)


### Disk QoS

En esta sección se indican las limitaciones que se pueden poner a los discos.

Para añadir un límite de disco, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains21.png)

Y se rellena el formulario

![](./domains.images/domains22.png)


### Remote VPNs

En esta sección se pueden añadir redes remotas a los escritorios.

Para añadir una red remota, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains23.png)

Y se rellena el formulario

![](./domains.images/domains24.png)


## Bookables



### Priority

En esta sección se pueden crear prioridades dependiendo de lo que se necesite en ese momento. Aquí se pueden crear reglas con un determinado tiempo máximo de reserva, número de escritorios, etc.

![](./domains.images/domains26.png)

Para añadir una prioridad nueva, se pulsa el botón ![](./domains.images/domains11.png)

![](./domains.images/domains27.png)

Y saldrá una ventana de diálogo donde se pueden rellenar los campos, también per,ite compartir con otros roles, categorías, grupos o usuarios:

* **Name**: El nombre que se le quiere dar a la prioridad

* **Description**: Una pequeña descripción para poder saber un poco más de información sobre esa prioridad

* **Rule**: Es la regla que le damos a esa prioridad, esto se aplica en el apartado de "Resources". Es importante tener en cuenta que si se quiere aplicar más de una regla a una prioridad, que se llamen iguales.

* **Priority**: Es la prioridad que se le da a la regla; cuánto mayor sea el número, más prioridad tendrá y cuánto menor sea el número, menos tendrá.

* **Forbid time**: Es el tiempo de antelación que no se pueden hacer reservas.

* **Max time**: Es el tiempo máximo de una reserva.

* **Max items**: Son el máximo de escritorios que se permite tener con dicha regla.

![](./domains.images/domains28.png)


#### Compartir 

Si se quiere compartir una prioridad con un rol, categoría, grupo o usuario se pulsa el icono ![](./domains.images/domains29.png)

![](./domains.images/domains30.png)

Y se rellena el formulario

![](./domains.images/domains31.png)


#### Editar

Para editar una prioridad, se pulsa el icono ![](./domains.images/domains32.png)

![](./domains.images/domains33.png)

Aparece una ventana de diálogo donde se pueden editar los campos:

![](./domains.images/domains34.png)


### Resources

En esta sección se puede encontrar todos los perfiles de GPUs que se hayan seleccionado en el apartado de "Hypervisores"

Aquí se le pueden aplicar las prioridades/reglas que se hayan creado anteriormente.

![](./domains.images/domains35.png)


#### Compartir

Para compartir un recurso, se pulsa el icono ![](./domains.images/domains29.png)

![](./domains.images/domains36.png)

Y se rellena el formulario

![](./domains.images/domains37.png)


#### Editar

Para editar un recurso, se pulsa el icono ![](./domains.images/domains32.png)

![](./domains.images/domains38.png)

Aparece una ventana de diálogo donde se pueden editar los campos:

![](./domains.images/domains39.png)


### Events 

En esta sección se pueden ver los eventos que se generan derivados de las acciones de las gpus.