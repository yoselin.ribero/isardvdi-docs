# Monitoring

!!! warning "Roles with access"
    Only **managers** have access to this feature. Managers are restricted to their own category.

This manual has been designed to provide **managers** users with a comprehensive overview of how to effectively monitor and adjust resource usage on the IsardVDI platform.

En los próximos apartados, exploraremos cómo utilizar las herramientas y características de monitorización disponibles para obtener información detallada sobre cómo se están utilizando los recursos en vuestro entorno. Desde el seguimiento del consumo de CPU y memoria hasta la gestión de horas consumidas por escritorio, este manual proporcionará los conocimientos necesarios para tomar decisiones informadas y optimizar vuestra configuración de IsardVDI.

In the next sections, we will explore how to use the available monitoring tools and features to obtain detailed information about how resources are being used in your environment. From tracking CPU and memory consumption to managing hours consumed per desktop, this manual will provide the knowledge you need to make informed decisions and optimize your IsardVDI configuration.

Our goal is to provide the tools and knowledge necessary to maximize the efficiency and performance of your virtual desktop environment.

To monitor the use of the platform, you must go to the Administration panel, press the button ![](./manage_user.images/manage_user26.png)

![](./manage_user.images/manage_user27.png){width="90%"}

Press the button ![](./usage.images/usage1.png)

![](./usage.images/usage2.png){width="40%"}

## The use of the platform

!!! Info "Key concepts"
    * **Item type**: They are the different types of elements that can consume resources within the system, they can be "desktop", "media", "storage" and "user".

    * **Parameter**: Each of the different consumptions recorded. For example, how many desktops have been booted, how many hours have they been booted, how much memory have they used, how much storage has been used, etc.

The main view of the platform's usage monitoring section offers information related to consumption in a specific period of time. The **end** column indicates with an arrow how this value has evolved (if it has increased or decreased) in the chosen time. The **cumulative** consumption will be shown, that is, the **consumption made since the beginning of the use of the platform**, therefore, it will always be increasing.

![](./usage.images/usage3.png)

!!! Tip "Suggestion"
    Instead of viewing the consumption from the beginning of using the platform, you can know specifically how each value increased/decreased in the chosen interval. It can be achieved in two ways:

    * Hovering over the arrow in the **end** column:

    ![](./usage.images/usage6.png)

    * Selecting the box ![](./usage.images/usage4.png):

    ![](./usage.images/usage5.png)

Several filters are available to adjust the information to the user's needs:

![](./usage.images/usage7.png)

* **Dates between**: Indicates the time period to view.
* **Grouping**: Defines the grouping of parameters to display.

!!! Warning "Atention"
    * **Grouping** depends on the item type:

    ![](./usage.images/usage8.png)
* **Consumer**: Depending on the type of element chosen, consumption can be broken down in different ways. That is to say:
    * For a **desktop** we can see the consumption by category, group, deployment, template, user and individually for each desktop.
    * For **media**, **storage** or **users** we can see consumption by category, group and individually by user.
* **Group**: Specifically indicates the element to be displayed. If no value is chosen, it shows the information of all available elements.

![](./usage.images/usage9.png)


## Generate graphs

You can select several elements by checking the boxes on the left and graphically view their consumption by pressing the button ![](./usage.images/usage10.png)

![](./usage.images/usage11.png)

A window will open with a graph where the evolution of the different parameters in the chosen time interval is displayed. In addition, below the graph there is a table with the information broken down day by day. The **cumulative** consumption will be shown, that is, the **consumption made since the beginning of the use of the platform**, therefore, the graph will always be increasing:

![](./usage.images/usage12.png)

!!! Warning "Atention"
    * The selected **grouping** may have certain limitations associated with it, these will be shown when drawing its graph:

    ![](./usage.images/usage20.png)

    Type of associated limitations:

    * **Expected use area**: Calculated area of ​​average consumption. That is, the expected use of the platform.
    * **Soft limit**: Upon reaching this limitation, the user will be informed so that they can reduce the consumption of the platform.
    * **Hard limit**: Upon reaching this limitation, use of the system will be blocked.


You can know specifically how each parameter has increased/decreased on a daily basis by clicking on the box ![](./usage.images/usage13.png)

![](./usage.images/usage14.png)

### Values display

You can view the values ​​of the different parameters at a specific point on the graph by moving the mouse over it:

![](./usage.images/usage15.png)

You can select/deselect the parameters to be displayed in the graph view by clicking on each of the names located at the top of the graph:

![](./usage.images/usage16.png)

### View type

You can change the view from **line** graph to **bar** graph by clicking on the corresponding icons:

![](./usage.images/usage17.png)

### Download

#### Image

You can download the generated graph as an image:

![](./usage.images/usage18.png)

#### Table

You can download the table with the information broken down day by day:

![](./usage.images/usage19.png)
