# Manage users

To manage users you have to go to the Administration panel, press the button ![](./manage_user.images/manage_user26.png)

![](./manage_user.images/manage_user27.png)

Press the button ![](./manage_user.images/manage_user24.png)

![](./manage_user.images/manage_user25.png)

## Group Creation

To create groups you have to go to the "Management" subsection under the "User" section in the "Administration" panel. Then look for the "Groups" table and press the button ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user2.png)

And fill the fields in the form.


![](./manage_user.images/manage_user3.png)

!!! Info "Important notes about the form fields"
    * **Linked groups**: All the resources that have been shared with its linked groups will be automatically inherited by the created group. For instance, if a Group A is created with Group B as a linked group, all the resources shared with Group B will also be shared with Group A.

    * **Auto desktops creation**: When the group users log in a desktop with the selected template will be created automatically.

    * **Ephimeral desktops**: Will set a limited time of use to a temporary desktop. (For this you also have to configure, being Admin, in Config the "Job Scheduler")


## Group Edition

A group's parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the group we want to update, and then pressing ![](./manage_user.images/manage_user30.png).

![](./manage_user.images/manage_user31.png)

A dialog box will pop up with the same parameters as the creation one.

![](./manage_user.images/manage_user29.png)

## User Creation

To create users you have to go to the "Management" subsection under the "User" section in the "Administration" panel.

### Individually

Look for the "Users" table and press the button ![](./manage_user.images/manage_user1.png)

![](./manage_user.images/manage_user5.png)

And a dialog box will appear with the following form: 

![](./manage_user.images/manage_user6.png)

And fill the fields in the form

!!! Info "Important notes about the form fields"
    * **Secondary groups**: All the resources that have been shared with its secondary groups will be automatically inherited by the created user. In addition, the user will be added to the deployments created in any of its secondary groups.

### Bulk Creation

Look for the "Users" table and press the button ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

And a dialog box will appear with the following form: 

![](./manage_user.images/manage_user9.png)

An example file can be downloaded by clicking the button ![](./manage_user.images/manage_user10.png). A form will appear to fill out:

!!! Warning "Avoiding errors"
    * The **category** and **group** fields must **exactly** match its name 
    * The recommended encoding is **Unicode (UTF-8)**
    * The csv must be separated by **commas** ","
    * It's highly recommended to use the given sample csv

![](./manage_user.images/manage_user11.png)

Once filled out, it can be uploaded on

![](./manage_user.images/manage_user12.png)

And if it has been uploaded correctly, a preview table of the users will appear:

![](./manage_user.images/manage_user28.png)

!!! Failure "Errors"
    If the csv has not been uploaded correctly, an error will be shown indicating the reason.


## User Edition

To edit users you have to go to the "Management" subsection under the "User" section in the "Administration" panel.

### Individually

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to update, and then pressing ![](./manage_user.images/manage_user33.png).

![](./manage_user.images/manage_user34.png)

A dialog box will pop up with the same parameters as the creation one.

![](./manage_user.images/manage_user35.png)

!!! Info "Important notes about the form fields"
    * The users **username**, **group** and **category** can't be modified.

### Bulk Edition

Look for the "Users" table and press the button ![](./manage_user.images/manage_user7.png)

![](./manage_user.images/manage_user8.png)

And a dialog box will appear with the following form: 

![](./manage_user.images/manage_user9.png)

Where a csv file with the updated data can be uploaded. Then select the checkbox "Update existing users"

![](./manage_user.images/manage_user36.png)

!!! Warning "Avoiding errors"
    * The **username**, **category** and **group** fields must **exactly** match its name in order to update the user
    * The recommended encoding is **Unicode (UTF-8)**
    * The csv must be separated by **commas** ","

!!! Info "Important notes about the form fields"
    * The users **username**, **group** and **category** can't be modified. The rest of the fields will be updated
    * The users **will be added to all** the selected secondary groups


### Enable/Disable User

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to disable/enable, then press ![](./manage_user.images/manage_user42.png).

![](./manage_user.images/manage_user37.png)

The users status can be seen in the users table.

![](./manage_user.images/manage_user38.png)

!!! Danger "Be careful"
    * If the user is disabled while logged in it could provoke session errors.

### Change User Password

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to change its password, then press ![](./manage_user.images/manage_user43.png).

![](./manage_user.images/manage_user39.png)

A dialog box will pop up with the following form:

![](./manage_user.images/manage_user40.png)

!!! Tip
    * Users can change its passwords through their [profile](../user/profile.md).

### Impersonate User

Users parameters can be edited by pressing the icon ![](./manage_user.images/manage_user13.png) next to the user we want to impersonate.

![](./manage_user.images/manage_user41.png)

!!! Danger "Disclaimer"
    Impersonating a user **provides access to all its data and desktops and it comes with inherent risks**. Before proceeding, it is important to **consider the sensitivity of the information that will be accessed**.

## Enrollment keys

In the "Groups" table search for the group that you want to obtain the code. Press the button ![](./manage_user.images/manage_user13.png) to view the group details

![](./manage_user.images/manage_user14.png)

The group is displayed with some options. Press the button ![](./manage_user.images/manage_user15.png)

![](./manage_user.images/manage_user16.png)

A dialog box will appear where you can generate codes by clicking the different checkboxes.

![](./manage_user.images/manage_user32.png)

Once the enrollment key code is generated, it can be copied and shared with users.

![](./manage_user.images/manage_user17.png)

!!! Danger "Be careful"
    * When signing up, **the users will be given the role of the shared enrollment key code**. For instance, Teachers, will be given the "Advanced" code and students the "Users" code.
    * An unlimited amount of users can sign up using the given enrollment key code. Thus, it's recommended to deactivate them once used.
