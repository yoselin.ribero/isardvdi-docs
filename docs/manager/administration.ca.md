# Administració

!!! Info
    Només els **gestors** (manager) i els **administradors** de la plataforma tenen accés a aquesta secció del menú de l'usuari.
    
    Els gestors tenen restriccions en aquest apartat per veure només aspectes de la categoria a la què pertanyen.

**Administració** permet als gestors i administradors gestionar i supervisar als usuaris, escriptoris, plantilles, desplegaments i altres recursos del sistema.

L'usuari amb rol d'administrador també té accés a la secció **Descàrregues** d'IsardVDI, gestió d'hipervisors i configuració del sistema.

Al [**Manual d'administrador**](../../admin/index.ca/) es troben aquestes prestacions.
