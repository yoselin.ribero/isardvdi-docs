# Viewers

## Direct Software Download

<table>
  <thead>
    <tr>
      <th>Operating System</th>
      <th>SPICE Viewer</th>
      <th>RDP Viewer</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Windows</td>
      <td>
        <a href="https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi">virt-viewer 11</a>
      </td>
      <td>No installation needed</td>
    </tr>
    <tr>
      <td>Linux</td>
      <td>
        <code>sudo apt install virt-viewer / sudo dnf install virt-viewer</code>
      </td>
      <td><code>sudo apt install remmina / sudo dnf install remmina</code></td>
    </tr>
    <tr>
      <td>Mac OS</td>
      <td>You can follow this <a href="https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c">installation guide</a></td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Microsoft Remote Desktop</a></td>
    </tr>
    <tr>
      <td>Android</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE">aSPICE</a></td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">Remote Desktop</a></td>
    </tr>
    <tr>
      <td>Apple iOS</td>
      <td><a href="https://apps.apple.com/gb/app/aspice-pro/id1560593107">aSPICE Pro</a></td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">Remote Desktop Mobile</a></td>
    </tr>
  </tbody>
</table>


## SPICE

![](viewers.images/spice_viewer_1.png){width="30%"}


### Description

SPICE is a communication protocol for virtual environments that allows access to video, mouse, and keyboard signals to the desktop as if you were connected to the screen, mouse, and keyboard of a real machine.

* **Advantages**:
    * It is the virtual machine engine itself used by Isard (Qemu-KVM) that enables access, regardless of the operating system running.
    * This protocol can be used to view the entire boot sequence of the desktop from the beginning.
    * No additional components need to be installed on the virtual desktop operating system to enable interaction.
    * It is a bandwidth-optimized video protocol that compresses the signal and only sends the areas that change from one frame to another.

* **Disadvantages**:
    * The client is not installed by default on any operating system. The installation is straightforward and can be done by any user, but in corporate or educational environments, permission restrictions may hinder installation.
    * Installation on Windows requires an additional program to redirect USB ports.
    * The connection is made through an HTTP proxy using a "CONNECT" method. This method may be filtered by a firewall or intermediate proxy in some cases.

Notable features of this viewer:

* Low latency
* Integrated audio
* Option to connect devices


### How to use SPICE viewer

After [downloading the necessary software](#direct-software-download), when starting a desktop, select **SPICE Viewer** option and open the downloaded file. Once opened, you will be able to access the desktop.

![](viewers.es.images/visor_spice_3.png){width="80%"}


### Scanning USB devices

!!! Info
    **FOR WINDOWS HOSTS**
    
    If **virt-viewer 11** is installed, one of these steps should be done for scanning USB devices to be activated:

    - Option 1
        - Uninstall **virt-viewer 11** and install the **[virt-viewer 7 version](https://releases.pagure.org/virt-viewer/virt-viewer-x64-7.0.msi)** instead
        - Install package **[UsbDk](https://github.com/daynix/UsbDk/releases/download/v1.00-22/UsbDk_1.0.22_x64.msi)**

    - Option 2
        - Install package **[UsbDk](https://github.com/daynix/UsbDk/releases/download/v1.00-22/UsbDk_1.0.22_x64.msi)**
        - Replace "**libusb-1.0.dll**" file at "**C:\Program Files\VirtViewer v11.0-256**" with **[this one](https://nextcloud.isardvdi.com/s/jSngRESwbYZHRks)** instead


#### virt-viewer 7

Open the SPICE Viewer for the desktop and select the **File >> USB Device Selection** options and choose the device to scan.

![](viewers.es.images/visor_spice_4.png){width="70%"}

![](viewers.es.images/visor_spice_5.png){width="70%"}


#### virt-viewer 11 (Windows)

Open the SPICE Viewer for the desktop, click the viewers' window second button and choose the device to scan.

![](viewers.ca.images/visor_spice_3.png){width="80%"}

![](viewers.ca.images/visor_spice_4.png){width="70%"}


## VNC browser

![](viewers.images/browser_viewer_1.png){width="30%"}


### Description

NoVNC is a protocol that operates at the same level as SPICE, but being an older protocol, the video signal is not as optimal. It is the protocol used in the browser-based integrated viewer. Notable features of this viewer:

* Works with any modern web browser
* Medium latency
* Audio not available
* Device connection option not available


### How to use browser viewer

Select **VNC browser viewer** option from the available viewers of a started desktop.

!!! Info
    <span style="font-size: medium">It is important to note that the option to open pop-up windows may be blocked in the browser. If that's the case, the browser viewer will not open.
    In this case, for example, in Firefox, you can fix it as follows:</span>

    <span style="font-size: medium">A message will appear in the tab where you can update this setting:</span>

    ![](viewers.images/browser_viewer_3.png)

    ![](viewers.images/browser_viewer_4.png){width="50%"}

The desktop viewer will automatically open in a new tab.

![](viewers.images/browser_viewer_2.png){width="80%"}


## RDP

![](viewers.images/rdp_viewer_1.png){width="30%"}
![](viewers.images/rdp_viewer_8.png){width="30%"}
![](viewers.images/rdp_viewer_9.png){width="30%"}


### Description

#### Native RDP

RDP is the default protocol used to remotely connect to a Windows operating system computer. These viewers are not available from the desktop startup, and you have to wait for the desktop to obtain an IP address during the process.

* **Advantages**:
    * The best user experience if the virtual desktop operating system is Windows, as well as the host machine.
    * No additional software installation is required on Windows hosts, as the Remote Desktop Client is integrated by default in the operating system.
    * Necessary for a good user experience when using NVIDIA vGPUs on Windows operating systems.
  
* **Disadvantages**:
    * If there is any problem during the operating system startup, you won't have access to the screen signal (this can be resolved by connecting through SPICE viewers or in the browser).

Notable features of these viewers:

- Low latency (better experience on Windows OS desktops)
- Integrated audio
- Device connection option
- Better experience when using design applications that require vGPU
- Desktops with vGPU only work with RDP viewers


#### RDP Browser viewer

IsardVDI uses the *Guacamole* server, which allows HTML5 RDP clients (any modern browser) to connect to IsardVDI's Windows guests through the default HTTPS port. Additionally, it supports audio through the browser.

* Works with any modern browser
* Low latency
* Integrated audio
* Device connection option not available


### How to use RDP and RDP VPN viewers

As mentioned before, **the RDP viewer requires an IP address** to connect and establish an RDP connection. That's why when a desktop starts, you may see the RDP viewer links as non-selectable, indicating that an IP address is not yet available.

Once the desktop obtains an IP address, it is displayed to the user through the interface, and access to the RDP, RDP in the browser, and RDP VPN viewers is activated:

![](viewers.images/rdp_viewer_2.png){width="25%"}
![](viewers.images/rdp_viewer_3.png){width="25%"}

!!! Warning
    <span style="font-size: medium">The desktop must have the ***Wireguard VPN*** interface enabled for the RDP viewers to connect.</span>

!!! Info
    <span style="font-size: medium">The **RDP VPN** viewer is used exactly the same as the native RDP viewer, with the exception that the user needs to establish the **VPN connection** between IsardVDI and their personal device. See **[VPN usage documentation](https://isard.gitlab.io/isardvdi-docs/user/vpn/)** in the manual for more information.</span>


#### With Windows host

Firstly, Windows generally comes with the Remote Desktop Client integrated into newer systems.

When selecting the RDP or RDP VPN viewers, a file with the extension *.rdp* is downloaded. This file contains the information needed to connect to the desktop:

![](viewers.images/rdp_viewer_10.png){width="40%"}

The first time you connect via RDP on the host, it will display a security alert. You can bypass this alert by checking the "**Don't ask me again for connections to this computer**" box for future connections.

![](viewers.es.images/visor_rdp_11.png){width="60%"}

The default credentials to access the desktops provided by Isard are:

* Username: **isard**
* Password: **pirineus**

!!! Warning
    <span style="font-size: medium">These username and password correspond to the credentials of the user created in the virtual desktop's operating system that you want to access, so that the user can log in to the computer.</span>

    <span style="font-size: medium">If you have a desktop with a customized user, you should **[modify these credentials in the desktop from the Isard interface](../../edit_desktop/#rdp-login)**.</span>

![](viewers.es.images/visor_rdp_12.png){width="40%"}

Upon accessing the desktop, it will prompt you to confirm and accept the certificate:

![](viewers.es.images/visor_rdp_13.png){width="45%"}

Finally, the client opens, and you can interact with the desktop.


#### With Linux host

After downloading the Remmina program through the command line, when starting a desktop, select the **RDP Viewer** or **RDP VPN Viewer** options and open the downloaded file with the newly installed **Remmina** program. Once opened, you can access the desktop.

![](viewers.es.images/visor_rdp_6.png){width="80%"}

![](viewers.es.images/visor_rdp_7.png){width="80%"}


### Scanning USB devices

#### With Windows host

From the downloaded *.rdp* file when selecting **RDP Viewer** or **RDP VPN Viewer** from a running desktop, modify the file by right-clicking on its location.

![](viewers.es.images/visor_rdp_10.png){width="40%"}
![](viewers.es.images/visor_rdp_14.png){width="45%"}

**Local Resources >> More >> Select USB drive >> Connect**

![](viewers.es.images/visor_rdp_15.png){width="40%"}
![](viewers.es.images/visor_rdp_16.png){width="40%"}

![](viewers.es.images/visor_rdp_17.png){width="40%"}
![](viewers.es.images/visor_rdp_18.png){width="40%"}

When accessing the RDP viewer, the USB device will appear in the list of disks in the desktop.

![](viewers.es.images/visor_rdp_19.png){width="80%"}


#### With Linux host

Currently, it is **not possible to scan USB devices using the Remmina client** for Linux operating systems.


### How to use RDP Browser viewer

Similar to the **[noVNC viewer for the browser](#browser)**, the RDP viewer will open in a new browser tab with direct access to the desktop.

!!! Warning
    <span style="font-size: medium">The desktop must have the ***Wireguard VPN*** interface enabled for the RDP viewers to connect.</span>


## Enabling RDP

Just as RDP needs to be enabled on the host machine, as explained earlier in this guide, the RDP protocol also needs to be enabled within the operating system of the virtual desktops themselves.

!!! Tip
    <span style="font-size: medium">The **pre-designed templates offered by IsardVDI** already have the operating system **prepared for Wireguard connections** with the RDP viewers, so this section of the guide is **not necessary** if the user creates desktops based on these templates.</span>

    <span style="font-size: medium">However, if the user has created a **custom desktop on their own**, here's how to configure their system to access it using the RDP viewers.</span>


### For Windows Desktops

**1. Enable RDP**

![](viewers.es.images/guest_rdp_cfg1.png){width="70%"}

**2. Disable network level authentication**

![](viewers.es.images/guest_rdp_cfg2.png){width="40%"}

The desktop is now ready.


### For Linux desktops

You can follow **[this section of the manual for Ubuntu operating systems](../../../guests/ubuntu_22.04/desktop/activate_rdp/configuration.ca)**.


## Technical Details

In Virtual Desktop Infrastructure (VDI), viewers are important because they enable remote access to virtual desktops. VDI allows users to access a virtual desktop from any device with an internet connection, and viewers provide the interface for users to interact with that virtual desktop.

Viewers typically offer features such as display protocols, compression, multimedia redirection, USB device redirection, and authentication. They allow users to access the virtual desktop environment, use applications, and access data from a remote location as if they were physically sitting at the desktop.

Viewers also play a crucial role in ensuring that the virtual desktop experience closely resembles the host computer experience. They must be capable of handling high-resolution graphics, multimedia content, and other types of data without causing delays or interruptions in the user experience.

In summary, viewers are essential in VDI because they provide the means for users to access and interact with virtual desktops, and they play a crucial role in ensuring that the virtual desktop experience is smooth and efficient.

You can also enable the option of a **direct viewer**, which will provide you with a direct link to connect to your desktop without the need to authenticate within the system.


### Viewer's features table summary

<table>
  <thead>
    <tr>
      <th>IsardVDI Viewer</th>
      <th>SPICE Viewer</th>
      <th>Browser Viewer</th>
      <th>RDP Viewer</th>
      <th>RDP Browser viewer</th>
      <th>RDP VPN viewer</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><strong>Protocol</strong></td>
      <td>SPICE</td>
      <td>VNC-SPICE</td>
      <td>RDP</td>
      <td>RDP</td>
      <td>RDP</td>
    </tr>
    <tr>
      <td><strong>HTML5 web client</strong></td>
      <td>-</td>
      <td>NoVNC</td>
      <td>-</td>
      <td>Guacamole</td>
      <td>-</td>
    </tr>
    <tr>
      <td><strong>Windows viewer</strong></td>
      <td>Remote-viewer<br><a href="https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi">virt-viewer 11 installer</a></td>
      <td>Web Browser</td>
      <td>Remote Desktop Viewer (windows)</td>
      <td>Web Browser</td>
      <td>-</td>
    </tr>
    <tr>
      <td><strong>Linux viewer</strong></td>
      <td>sudo apt install virt-viewer<br>sudo dnf install remote-viewer</td>
      <td>Web Browser</td>
      <td>Remmina</td>
      <td>Web Browser</td>
      <td>Remmina</td>
    </tr>
    <tr>
      <td><strong>Mac Viewer</strong></td>
      <td>You can follow the <a href="https://gist.github.com/tomdaley92/789688fc68e77477d468f7b9e59af51c">installation guide</a></td>
      <td>Web Browser</td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Apple Store</a></td>
      <td>Web Browser</td>
      <td><a href="https://apps.apple.com/app/microsoft-remote-desktop/id1295203466?mt=12">Apple Store</a></td>
    </tr>
    <tr>
      <td><strong>Android Viewer</strong></td>
      <td><a href="https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE">Free version</a> on the Play Store and also a <a href="https://play.google.com/store/apps/details?id=com.iiordanov.aSPICE">paid version</a> with more features.</td>
      <td>Web Browser</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">remote desktop</a></td>
      <td>Web Browser</td>
      <td><a href="https://play.google.com/store/apps/details?id=com.microsoft.rdc.androidx">remote desktop</a></td>
    </tr>
    <tr>
      <td><strong>Apple iOS Viewer</strong></td>
      <td><a href="https://apps.apple.com/gb/app/aspice-pro/id1560593107">Paid version</a> on Apple Store</td>
      <td>-</td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">remote desktop</a></td>
      <td>-</td>
      <td><a href="https://apps.apple.com/us/app/remote-desktop-mobile/id714464092">remote desktop</a></td>
    </tr>
    <tr>
      <td><strong>Download File Extension</strong></td>
      <td>*.vv</td>
      <td>Web Browser</td>
      <td>*.rdp</td>
      <td>-</td>
      <td>*.rdp</td>
    </tr>
    <tr>
      <td><strong>Copy / Paste real desktop to virtual desktop</strong></td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Wireguard client required to access user VPN</strong></td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>USB Redirection</strong></td>
      <td>✓ <br>(with windows you need to install usb)</td>
      <td>✗</td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Audio</strong></td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
      <td>✗</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>Adapt desktop screen size to viewer</strong></td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
    <tr>
      <td><strong>You can see how the desktop boots</strong></td>
      <td>✓</td>
      <td>✓</td>
      <td>✗</td>
      <td>✗</td>
      <td>✗</td>
    </tr>
    <tr>
      <td><strong>You must have IP address and RDP service on your virtual desktop</strong></td>
      <td>✗</td>
      <td>✗</td>
      <td>✓</td>
      <td>✓</td>
      <td>✓</td>
    </tr>
  </tbody>
</table>


### Operating System compatibility table summary 

<table>
  <thead>
    <tr>
      <th>VDI Viewer</th>
      <th>Supported OS</th>
      <th>Protocols</th>
      <th>Multimedia Redirection</th>
      <th>USB Redirection</th>
      <th>Video Compression</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Microsoft Remote Desktop</td>
      <td>Windows, Mac, iOS, Android</td>
      <td>RDP</td>
      <td>✓</td>
      <td>✓</td>
      <td>RemoteFX</td>
    </tr>
    <tr>
      <td>Spice</td>
      <td>Windows, Linux</td>
      <td>SPICE</td>
      <td>✓</td>
      <td>✓</td>
      <td>Lossless and Lossy</td>
    </tr>
    <tr>
      <td>VNC</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>VNC</td>
      <td>✗</td>
      <td>✓</td>
      <td>Tight and Zlib</td>
    </tr>
    <tr>
      <td>Guacamole HTML5 Client</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>RDP, VNC, SSH</td>
      <td>✓</td>
      <td>✓</td>
      <td>JPEG, PNG</td>
    </tr>
    <tr>
      <td>noVNC Client</td>
      <td>Windows, Mac, Linux, iOS, Android</td>
      <td>VNC</td>
      <td>✓</td>
      <td>✓</td>
      <td>Tight, Hextile, CopyRect, Raw</td>
    </tr>
  </tbody>
</table>


### Differences between in-browser and desktop client viewers

There are two main types of viewers that can be distinguished:

* **In-browser viewers**: The viewer is integrated within a web page. The control of the desktop operating systems can be done from a browser tab. Video signal decoding, mouse input, and keyboard input are handled within the browser tab.

    * **Advantages**:
        * No need to install a separate client; works on any device (computers, tablets, mobile devices) and operating system that has a web browser.
    * **Disadvantages**:
        * Decoding efficiency is not as high as in a dedicated viewer, which can result in slower desktop refresh and slower performance when moving elements within the viewer.
        * Local USB devices cannot be redirected to the virtual desktop.

* **Desktop client applications**: These are dedicated viewer applications that are optimized to provide the best user experience.

    * **Advantages**:
        * Optimized for desktop client protocols, offering better performance compared to in-browser viewers. With no network issues (appropriate latency and bandwidth), the desktop experience feels similar to using a physical computer.
        * Depending on the protocol type and client version, these viewers often provide advanced features such as USB device redirection and file drag-and-drop between the virtual desktop and the host machine.
    * **Disadvantages**:
        * The client applications may not come pre-installed with the operating system, requiring an additional software installation on the host machine.
        * Advanced features may be limited based on client versions and protocol versions.


### Ports and Proxies

In IsardVDI, significant effort has been made to avoid the need for opening extra ports and to encapsulate connections through HTTP proxies. The platform has been designed to adapt to various situations where a firewall may block or hinder connections. By default, the following ports are used:

* TCP/**80** for the proxy that encapsulates **SPICE** protocol connections.
* TCP/**443** for the web interface and **in-browser** integrated viewers.
* TCP/**9999** for the proxy that encapsulates **RDP** protocol connections.


## FAQs - Frequently Asked Questions

<details>
  <summary>I am unable to scan a USB device using an RDP viewer</summary>
  <p>If a user with a Windows host is unable to scan a USB device in a virtual desktop, they can refer to <b><a href="../remote_fx">this guide on using RemoteFX</a></b>.</p>
</details>
