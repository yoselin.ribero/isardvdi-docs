# Networks

Desktops in IsardVDI can have multiple network cards at the same time and can be of different kinds:

- **Default**: Provides dynamic address asignment (DHCP) with DNS and Gateway through a NAT router. It has Internet connection and it is isolated from other desktops in system.
- **Personal**: Personal networks kind attached to user desktops will be in the same network (VLAN). With personal networks, users can set up a private network environment for their virtual machines, where they can assign their own IP addresses, configure network settings, and control access to the network. This can be useful for scenarios such as testing or development environments, where multiple user virtual desktops need to interact with each other in a closed environment.
- **Private (OVS)**: **OpenVSwitch** networks allows to connect in the same network (VLAN) all the virtual desktops that have it attached, regardless of the virtual desktop owner. They can also be connected to the same VLAN network in infrastructure, thus allowing to connect physical hosts to virtual desktops. **This option has to be setup at server level. (Read isardvdi.cfg for setup options)**.
- **Wireguard VPN**: This is a system network that assigns addressing to the virtual desktop and it is needed for connecting to the desktop from home and to allow **RDP** type viewer connections.

!!! Info
    The [user with **administrator** role creates the networks](../admin/domains.md/#interfaces) of the system and all the other users must ask permission to create new ones.

Use cases involving different types of network interfaces:

- [**Client - Server environment**](../use_cases/client_server/client_server.md): how to create a **Private OVS** network and use it between an advanced role and a user role.
- [**Network tests**](../use_cases/network_tests/network_tests.md): introduction of different IsardVDI networks and how to setup in Linux virtual desktops.
- [**Personal networks**](../use_cases/private_and_personal_networks/private_and_personal_networks.md): how to use the private networks and the user personal networks (from a user with **administrator** role).

