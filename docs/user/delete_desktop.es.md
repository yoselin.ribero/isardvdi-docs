# Eliminar escritorio

Para eliminar un escritorio se pulsa el botón ![](./delete_desktop.es.images/delete_desktop1.png)

![](./delete_desktop.es.images/delete_desktop2.png){width="80%"}

![](./delete_desktop.es.images/delete_desktop3.png){width="80%"}

