# Redes

Los escritorios del IsardVDI pueden tener varias tarjetas de red a la vez y pueden ser de diferentes tipos:

- **Default**: Proporciona una dirección dinámica (DHCP) con DNS y pasarela a través de un router NAT. Tiene conexión a Internet y está aislado otros escritorios del sistema.
- **Personal**: El tipo de red personal adjuntado a los escritorios de usuario estará en la misma red (*VLAN). Con redes personales, los usuarios pueden configurar un en torno a red privado para sus máquinas virtuales, donde pueden asignar sus propias direcciones IP, configurar la configuración de la red y controlar el acceso a la red. Esto puede ser útil para escenarios como pruebas o entornos de desarrollo, donde múltiples escritorios virtuales de usuario necesitan interactuar entre ellos en un entorno cerrado.
- **Private (OVS)**: Las redes **OpenVSwitch** permiten conectar en la misma red (VLAN) todos los escritorios virtuales que lo hayan conectado, independientemente del propietario del escritorio virtual. También se pueden conectar en la misma red VLAN en infraestructura, permitiendo así conectar los ordenadores físicos a escritorios virtuales. **Esta opción se tiene que configurar a nivel de servidor. (Mirar isardvdi.cfg para las opciones de configuración)**.
- **wireguard VPN**: Esta es una red del sistema que asigna direcciones al escritorio virtual y es necesaria para conectarse al escritorio desde casa y permitir conexiones de visualización de tipo **RDP**.

!!! Info
    Las [redes las crea el usuario con rol **adminitrador**](../admin/domains.es.md/#interfaces) del sistema y el resto de usuarios deberán pedir permiso para crear nuevas.

Casos de uso con diferentes tipos de interfaces de red:

- [**Cliente - Entorno de servidor**](../use_cases/client_server/client_server.es.md): como crear una red **Private (OVS)** y utilizarla entre el rol **avanzado** y el rol de **usuario**.
- [**Pruebas de red**](../use_cases/network_tests/network_tests.es.md): se introducen diferentes redes y como configurarlas en los escritorios virtuales Linux.
- [**Redes personales**](../use_cases/private_and_personal_networks/private_and_personal_networks.es.md): como utilizar las redes privadas y las redes personales del usuario (desde un usuario con rol **administrador**).
