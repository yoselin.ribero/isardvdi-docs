# Reserves

En aquest apartat veurem com fer reserves per als escriptoris amb tecnologia vGPU associades a recursos GPUs de targetes Nvidia, com afegir aquest tipus de maquinari als escriptoris virtuals i la gestió de les reserves associada.

Per més informació, consultar l'[apartat de GPU](../deploy/gpus/gpus.ca.md).


## Per què un sistema de reserves

IsardVDI està pensat perquè els usuaris de forma autònoma puguin crear i desplegar escriptoris, el límit de quant escriptoris poden córrer de forma simultània en el sistema ve condicionat pels recursos disponibles. Si els escriptoris no tenen vGPUs, el límit sol venir per la quantitat de memòria RAM disponible en els hipervisors, essent aconsellable que sempre la capacitat de memòria i vCPUs d'aquests servidors sigui suficient per sostenir la concorrència d'escriptoris. Podem limitar la quantitat de recursos que atorguem a cada usuari, grup o categoria amb les quotes i límits que IsardVDI permet establir.

En el cas de les vGPUs, són recursos cars (hi ha el cost de la targeta i el cost del llicenciament) i per accedir a aquests recursos s'ha establert un sistema de reserves, amb les següents característiques:

- L'administrador pot **planificar horaris amb diferents perfils** aplicats per a cada targeta disponible, permetent que en un determinat horari, s'estableixi un perfil amb molta memòria dedicada i pocs usuaris (p. ex.: llançar una renderització), i en un altre horari, un altre perfil amb poca memòria i molts usuaris (p. ex.: realitzar una formació).

- Existeix un **sistema de permisos** granular que permet definir quins usuaris tenen permís per usar un determinat perfil d'una targeta. Permet que només un grup reduït d'usuaris pugui accedir als perfils amb més memòria.

- Un usuari pot reservar un escriptori per a una determinada franja horària amb un **temps mínim previ** a la reserva i una **durada màxima de la reserva**. Permet que es puguin fer reserves amb poc temps d'antelació i que no es puguin reservar franges superiors a un nombre reduït d'hores.

- Un usuari avançat pot fer una reserva **per a un desplegament**, reservant tantes unitats de perfils de GPU com escriptoris conté el desplegament.

- Existeix també un sistema de **prioritats de reserves**, que permet posar regles perquè uns usuaris puguin sobreescriure i revocar reserves d'altres. Aquesta opció és útil si volem fomentar que la targeta GPU s'utilitzi per molts usuaris, però volem alhora garantir que per a uns determinats grups mai els falti capacitat de reserves, com per exemple, si un grup


## Preparar escriptori o desplegament

### Preparar item perquè funcioni amb GPU

[Crear](create_desktop.ca.md)/[Editar](edit_desktop.ca.md) escriptori o [crear nou desplegament](../advanced/deployments.ca.md) i desplaçar-se fins a arribar a l'apartat **Hardware**.

És important que la configuració **Vídeos** tingui l'opció **Only GPU** marcada. En els escriptoris amb S.O. Windows amb els drivers de Nvidia instal·lats, no és compatible tenir alhora una targeta de vídeo QXL i una targeta vGPU.

![](bookings.images/hardware_videos_only_gpu.png)

En l'apartat següent, **Reservables**, més avall, s'assigna el **perfil** de GPU desitjat.

![](bookings.images/bookables_gpu_dropdown.png)

NOTA: *Per poder reservar i iniciar un escriptori amb un perfil, l'administrador ha de planificar-ho prèviament.*


### Només visors RDP

Els escriptoris amb GPU fan ús del controlador de targeta GPU i per tant **només és possible connectar-se a ells mitjançant els visors RDP**.

![](bookings.images/rdp_viewers.png)

**IMPORTANT!** Si hi ha un problema amb el sistema operatiu i mai arribem a tenir adreça IP, no podrem accedir per RDP. Sempre cal evitar això, però, amb més raó en aquest apartat, forçar l'apagat de l'escriptori des de la interfície d'IsardVDI, perquè pot provocar una corrupció del disc (ja que és l'equivalent a treure el cable de corrent d'un ordinador encès) i que aquest no aconsegueixi iniciar el sistema operatiu i obtenir adreça IP, amb la qual cosa ens podrem quedar sense accés a l'escriptori.


## Reservar un escriptori o desplegament

És necessari realitzar les **reserves** dels **escriptoris** amb vGPU. L'administrador haurà planificat uns perfils de GPU en les diferents targetes disponibles i l'usuari podrà fer reserves sempre que tingui permisos i coincideixi el perfil del seu vGPU amb una planificació disponible.

Per a la gestió de reserves en escriptoris o desplegaments, es pot accedir als corresponents manuals o s'expliquen com crear i esborrar reserves d'aquests dos items.

!!! Documentació
    - **[Reserves en escriptoris](../book_desktop.ca)**
    - **[Reserves en desplegaments](../../advanced/deployments.ca/#reservar-desplegament)**
