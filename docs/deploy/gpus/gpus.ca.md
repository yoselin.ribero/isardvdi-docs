
# Explicació de la tecnologia vGPU

IsardVDI permet treballar amb la tecnologia de **[Nvidia vWS](https://www.nvidia.com/es-es/design-visualization/virtual-workstation/)** que permet disposar de recursos d'una targeta gràfica associats a un escriptori virtual. Aquesta tecnologia ens permet executar programari que requereix **recursos de GPU dedicats**, com ara programes de disseny 3D, animació, edició de vídeo, CAD o disseny industrial.

Cada targeta gràfica es pot dividir en diferents perfils amb una quantitat de memòria reservada. En funció del perfil seleccionat, es disposa de més o menys targetes virtuals. En la següent imatge es mostra un servidor amb una sèrie de targetes GPU instal·lades. Cada targeta es pot dividir en vGPUs (virtual GPUs) que es poden mapejar a cada màquina virtual.

![](./gpus.images/o6SWmXS.png)

Veurem amb un exemple el funcionament. Es disposa d'una targeta **[Nvidia A40](https://www.nvidia.com/es-es/data-center/a40/)** i es vol utilitzar un programa com el SolidWorks per dissenyar peces 3D. S'utilitza un perfil amb 2 GB de memòria gràfica dedicada. Aquesta targeta gràfica disposa de 48 GB de memòria, que es pot dividir en perfils "2Q": el primer caràcter "2" fa referència a la quantitat de memòria reservada (2 GB) i el segon caràcter "Q" fa referència a la llicència vWS, el mode en què la targeta virtual es configura per a treballs creatius i tècnics que utilitzen la tecnologia **[Quadro de Nvidia](https://docs.nvidia.com/grid/10.0/grid-vgpu-user-guide/index.html#supported-gpus-grid-vgpu)**. Els diferents perfils i modes de funcionament disponibles per a aquesta targeta estan accessibles al [manual de Nvidia amb els perfils per a l'A40](https://docs.nvidia.com/grid/13.0/grid-vgpu-user-guide/index.html#vgpu-types-nvidia-a40). En el cas de 2Q tenim:

- Memòria dedicada i reservada: 2 GB
- Nombre màxim de vGPUs per GPU: 24

Diagrames de funcionament de la tecnologia Nvidia vGPU:

![](./gpus.images/9qceuMB.png)

![](./gpus.images/qtAbMD3.png)

Per a més informació sobre l'ús de la tecnologia GPU sobre escriptoris en IsardVDI, consultar l'[apartat de reserves i perfils](../../user/bookings.ca.md).