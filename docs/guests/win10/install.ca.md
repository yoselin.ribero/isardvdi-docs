# Guia d'instal·ació Windows 10

## Pre-instal·lació

1. Es descarrega la imatge ISO de [Windows 10](https://www.microsoft.com/es-es/software-download/windows10ISO) per la versió de 64 bits.

2. Es [crea un escriptori a partir d'aquest mitjà](../../../advanced/media.ca/#crear-nou-escriptori-a-partir-de-mitja) amb el següent maquinari virtual:

    - Visors: **SPICE**
    - vCPUS: **4**
    - Memòria (GB): **8**
    - Videos: **Default**
    - Boot: **CD/DVD**
    - Disk Bus: **Default**
    - Mida del disc (GB): **100**
    - Xarxes: **Default**
    - Plantilla: **Microsoft windows 10 with Virtio devices UEFI**

3. S'[edita l'escriptori i s'assignen més mitjans](../../../user/edit_desktop.ca/#media), de tal forma que acabi tenint els següents:

    - **Win10_22H2_ES** (instal·lador)
    - **virtio-win-X** (controladors)
    - **Optimization Tools** (programari d'optimizació per a S.O. Windows)


## Instalación

### Windows 10 Pro

1. S'**envia "Ctr+Alt+Supr"** i es polsa qualsevol tecla del teclat a la segona pantalla

    ![](install.es.images/1.png){width="46%"}
    ![](install.es.images/2.png){width="45%"}

2. Tipus d'instal·lació

    ![](install.es.images/3.png){width="45%"}
    ![](install.es.images/4.png){width="45%"}

    ![](install.es.images/5.png){width="45%"}
    ![](install.es.images/6.png){width="45%"}

    ![](install.es.images/7.png){width="45%"}

3. Carregar controladors de sistema operatiu

    ![](install.es.images/8.png){width="45%"}
    ![](install.es.images/9.png){width="45%"}

    ![](install.es.images/10.png){width="45%"}
    ![](install.es.images/11.png){width="45%"}

    ![](install.es.images/12.png){width="45%"}


### Sistema

1. S'apaga i [edita l'escriptori](../../../user/edit_desktop.ca/#hardware) per a modificar el maquinari virtual **Boot** de l'opció **CD/DVD** a **Hard Disk**

2. S'arrenca l'escriptori, trigarà uns segons en iniciar el sistema operatiu

    ![](install.es.images/13.png){width="45%"}

3. Es seleccionen les següents opcions als següents pasos de l'agent

    ![](install.es.images/14.png){width="45%"}
    ![](install.es.images/15.png){width="45%"}

    ![](install.es.images/16.png){width="45%"}
    ![](install.es.images/17.png){width="45%"}

    ![](install.es.images/18.png){width="45%"}
    ![](install.es.images/19.png){width="45%"}

    ![](install.es.images/20.png){width="45%"}
    ![](install.es.images/21.png){width="45%"}

    ![](install.es.images/22.png){width="45%"}
    ![](install.es.images/23.png){width="45%"}

    ![](install.es.images/24.png){width="45%"}
    ![](install.es.images/25.png){width="45%"}

    ![](install.es.images/26.png){width="45%"}
    ![](install.es.images/27.png){width="45%"}

    ![](install.es.images/28.png){width="45%"}
    ![](install.es.images/29.png){width="45%"}

    ![](install.es.images/30.png){width="45%"}
    ![](install.es.images/31.png){width="45%"}

4. S'apaga l'escriptori i s'edita per a deixar-li només els següents mitjans (si es vol obviar aquest pas, igualment s'haurà d'apagar l'escriptori i tornar a iniciar-lo, **no pas reiniciar-lo**):

    - **virtio-win-X** (controladors)
    - **Optimization Tools** (programari d'optimizació per a S.O. Windows)


## Configuració

### Actualitzar i instal·lar

1. S'instal·len els dos controladors **virtio** del mitjà de l'escriptori. Ambdues instal·lacions són ràpidas i només s'ha de fer clic a **Next**

    ![](install.es.images/32.png){width="45%"}
    ![](install.es.images/33.png){width="45%"}

    ![](install.es.images/34.png){width="45%"}
    ![](install.es.images/35.png){width="45%"}

    ![](install.es.images/36.png){width="80%"}

2. Es comproven les **actualitzacions del sistema**, les cuals trigaran bastant en descarregar-se i instal·lar-se, per estar al dia amb la última versió de Windows Server (segurament es necessiti reiniciar el sistema múltiples vegades en la recerca de noves actualitzacions).

    ![](install.es.images/41.png){width="80%"}

3. S'instal·len els programes següents i es guarden els instal·ladors a la carpeta **admin** nova a la ruta **C:\admin**

    - Firefox
    - Google chrome
    - Libre Office
    - Gimp
    - Inkscape
    - LibreCAD
    - Geany
    - Adobe Acrobat Reader


### Usuari admin i canvi de permisos

A una **Powershell** amb permisos d'administrador:

1. Crear usuari **admin** al grup **Administradores**
    ```
    $Password = Read-Host -AsSecureString
    New-LocalUser "admin" -Password $Password -FullName "admin"
    Add-LocalGroupMember -Group "Administradores" -Member "admin"
    ```

2. Crear usuari **user** al grup **Usuarios**
    ```
    New-LocalUser "user" -Password $Password -FullName "user"
    Add-LocalGroupMember -Group "Usuarios" -Member "user"
    ```

2. Se li canvien els permisos a la carpeta **C:\admin** 

    ![](install.es.images/folder_admin_1.png){width="60%"}

    Es **deshabilita l'herència** de la carpeta i es marca la primera opció.

    ![](install.es.images/folder_admin_2.png){width="60%"}

    Es **treuen** els altres usuaris amb permisos per a només deixar **Administradores**

    ![](install.es.images/folder_admin_3.png){width="60%"}


### Desinstal·lar aplicacions i modificar configuracions de Microsoft 

1. A una **CMD** amb permisos d'administrador, es desinstal·la **Microsoft Edge**:

    ```
    cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\9*\Installer
    setup --uninstall --force-uninstall --system-level
    ```

2. A una **Powershell** amb permisos d'administrador, es desinstal·lan els següents paquets i programes:

    ```
    Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
    Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue
    ```

3. S'obre **msconfig** mitjançant les tecles **Windows + R** o des d'una **CMD**, i es desactiven els serveis següents (es reinicia l'equip si ho demana):

    - Administrador de autenticación Xbox Live
    - Centro de seguridad
    - Firewall de Windows Defender
    - Mozilla Maintenance Service
    - Servicio de antivirus de Microsoft Defender
    - Servicio de Windows Update Medic
    - Windows Update
    - Adobe Acrobat Update Service
    - Servicio de Google Update (gupdate)
    - Servicio de Google Update (gupdatem)
    - Google Chrome Elevation Service

4. S'obre l'**Administrador de tareas** i a la pestanya **Inicio** es **deshabilita** (es reinicia l'equip si ho demana):

    - Cortana
    - Microsoft OneDrive
    - Windows Security notifications

5. Es treuen els elements que hi surten a la dreta al menú **Inicio**, **Productividad** i **Explorar**:

    ![](install.es.images/37.png){width="50%"}

6. Es desactiven les notificacions de Windows a **Configuración - Sistema - Notificaciones y acciones**

    ![](install.es.images/38.png){width="50%"}

7. Es habiliten les connexions per **Escriptori Remot**. S'habilita la primera casella, i se deshabilita la segona casella a **Configuración avanzada**

    ![](install.es.images/39.png){width="50%"}

    ![](../win20192022/install.es.images/21-es.png){width="38%"}
    ![](../win20192022/install.es.images/22-es.png){width="39%"}

8. S'apaga i [edita l'escriptori](../../../user/edit_desktop.ca) amb el següent hardware virtual:

    - Visors: **RDP** i **RDP al navegador**
    - Login RDP:
        - Usuari: **isard**
        - Contrasenya: **pirineus**
    - vCPUS: **4**
    - Memòria (GB): **8**
    - Videos: **Default**
    - Boot: **Hard Disk**
    - Disk Bus: **Default**
    - Xarxes: **Default** i **Wireguard VPN**


### Autologon

1. Per habilitar l'inici de sessió automàtic conforme s'inicia el sistema, s'instal·la l'**[Autologon](https://learn.microsoft.com/en-us/sysinternals/downloads/autologon)**.

2. Es descomprimeix l'arxiu descarregat i s'executa **Autologon64**

3. S'escriu la contrasenya **pirineus**

    ![](install.es.images/40.png){width="50%"}


### Català (opcional)

Abans de passar les Optimization Tools, es canvia l'idioma al català seguint els passos a continuació; si no, es pot obviar aquest apartat.

1. A **Configuración - Hora e idioma - Idioma** s'afegeix l'**Idioma preferido** **Català** (es tanca la sessió si ho demana)

    ![](install.es.images/idioma_preferido.png){width="60%"}

    ![](install.es.images/42.png){width="25%"}
    ![](install.es.images/43.png){width="25%"}
    ![](install.es.images/44.png){width="25%"}

2. Es replica el canvi a tot el sistema (es reinicia l'equip si ho demana)

    ![](install.es.images/idioma_administrativo.png){width="60%"}

    ![](install.es.images/45.png){width="30%"}
    ![](install.es.images/46.png){width="29%"}


## Optimization tools

1. S'obre l'executable del mitjà assignat a l'escriptori

    ![](install.es.images/47.png){width="45%"}

2. Es fa clic al botó **Analizar** i seguidament **Opciones comunes**

    ![](install.es.images/48.png){width="45%"}
    ![](install.es.images/49.png){width="45%"}

3. S'assignen les opcions com a les següents imatges

    ![](install.es.images/50.png){width="45%"}
    ![](install.es.images/51.png){width="45%"}

    ![](install.es.images/52.png){width="45%"}
    ![](install.es.images/53.png){width="45%"}

    ![](install.es.images/54.png){width="45%"}
    ![](install.es.images/55.png){width="45%"}

    ![](install.es.images/56.png){width="45%"}
    ![](install.es.images/57.png){width="45%"}

4. (**SI L'IDIOMA CATALÀ ESTÀ DEFINIT, SI NO, OMITIR**) Es filtra per la paraula **idioma** i es desactiven les següents 3 opcions: 

    ![](install.es.images/58.png){width="45%"}

5. Es fa clic al botó **Optimizar** i s'espera què retorni una pantalla com la segona amb el resum del resultat

    ![](install.es.images/59.png){width="45%"}

6. Es reinicia el sistema
