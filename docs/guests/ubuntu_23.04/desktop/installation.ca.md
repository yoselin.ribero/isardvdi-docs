# Ubuntu Desktop 23.04 Lunar Lobster install

## Instal·lació del SO

**Selecció de l'idioma i de la distribució de teclat**

![](installation.images/3.png){width="45%"}
![](installation.images/5.png){width="45%"}


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](../../ubuntu_22.04/desktop/installation/images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```


## Configuració

### Terminal

**Comandaments bàsic**
```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim gedit vlc net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```

**Deshabilitar actualitzacions automàtiques i les notificacions de les noves**
```
$ sudo apt purge update-manager update-notifier*
$ sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::DevRelease "false";
$ sudo vim /etc/update-manager/release-upgrades
Prompt=never
$ sudo snap refresh --hold 
$ sudo snap set system refresh.retain=3
```


### Paràmetres

**Privacitat - Pantalla**

![](installation.images/privacy-screen_lock.png){width="70%"}

**Compartició - Escriptori remot**

![](installation.images/sharing-remote_desktop.png){width="70%"}

S'aplica també l'**[activació del RDP](../../ubuntu_22.04/desktop/activate_rdp/configuration.ca.md)**.

**Quant a - Actualitzacions de programari**

![](installation.images/about-software_updates.png){width="70%"}


## Personalització

**Fons**

![](installation.images/appearance.png){width="70%"}

**Usuaris**

![](installation.images/users.png){width="70%"}